import { ModelLoader } from './components/Canvas/ModelLoader'

function App() {
  return (
    <>
     <ModelLoader />
    </>
  )
}

export default App
