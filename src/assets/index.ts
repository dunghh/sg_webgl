import SHOES_01 from "./models/Shoes_01.glb";
import SHOES_02 from "./models/Shoes_02.glb";
import SHOES_03 from "./models/Shoes_03.glb";
import SHOES_04 from "./models/Shoes_04.glb";

import THUMBNAIL_01 from "./thumbnails/1.png";
import THUMBNAIL_02 from "./thumbnails/2.png";
import THUMBNAIL_03 from "./thumbnails/3.png";
import THUMBNAIL_04 from "./thumbnails/4.png";

const LIST_MODELS = [SHOES_01, SHOES_02, SHOES_03, SHOES_04];
const LIST_THUMBNAILS = [THUMBNAIL_01, THUMBNAIL_02, THUMBNAIL_03, THUMBNAIL_04];

export { LIST_MODELS, LIST_THUMBNAILS };
