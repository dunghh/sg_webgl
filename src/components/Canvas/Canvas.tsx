import React, { useEffect, useRef } from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

type CanvasProps = {
  model: THREE.Group | null;
};

const Canvas: React.FC<CanvasProps> = ({ model }) => {
  const mountRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!mountRef.current || !model) return;

    const mount = mountRef.current;
    const scene = new THREE.Scene();

    // Thiết lập camera
    const camera = new THREE.PerspectiveCamera(
      15, 
      mount.clientWidth / mount.clientHeight, 
      0.1, 
      1000
    );
    camera.position.set(1, 0.5, 0.5);

    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(mount.clientWidth, mount.clientHeight);
    renderer.setClearColor(0xCCCCCC); 
    mount.appendChild(renderer.domElement);

    const ambientLight = new THREE.AmbientLight(0xffffff, 0.6);
    scene.add(ambientLight);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(5, 10, 7.5);
    scene.add(directionalLight);

    scene.add(model);

    // Thiết lập điều khiển
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enableZoom = true;
    controls.autoRotate = true;
    controls.minDistance = 1;
    controls.maxDistance = 5;
    controls.mouseButtons = {
      LEFT: THREE.MOUSE.ROTATE,
      MIDDLE: THREE.MOUSE.DOLLY,
      RIGHT: THREE.MOUSE.ROTATE,
    }

    // Hàm xử lý khi thay đổi kích thước màn hình
    const handleResize = () => {
      camera.aspect = mount.clientWidth / mount.clientHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(mount.clientWidth, mount.clientHeight);
    };

    window.addEventListener('resize', handleResize);

    const animate = () => {
      requestAnimationFrame(animate);
      controls.update();
      renderer.render(scene, camera);
    };

    animate();

    return () => {
      window.removeEventListener('resize', handleResize);
      mount.removeChild(renderer.domElement);
      scene.remove(model);
    };
  }, [model]);

  return <div ref={mountRef} style={{ width: '100%', height: 'calc(100vh - 200px)' }} />;
};

export default Canvas;
