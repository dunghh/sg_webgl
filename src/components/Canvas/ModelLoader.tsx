import React, { useState, useEffect } from "react";
import * as THREE from "three";
import Canvas from "./Canvas";
import ARCanvas from "./ARCanvas";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader.js";
import { LIST_MODELS, LIST_THUMBNAILS } from "../../assets";
import Thumbnail from "./Thumbnail";

export const ModelLoader: React.FC = () => {
  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
  const [model, setModel] = useState<THREE.Group | null>(null);
  const [modelIndex, setModelIndex] = useState(0);

  useEffect(() => {
    const loader = new GLTFLoader();
    const dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath("/draco/");
    loader.setDRACOLoader(dracoLoader);

    loader.load(
      LIST_MODELS[modelIndex],
      (gltf) => {
        setModel(gltf.scene);
      },
      undefined,
      (error) => {
        console.error("An error happened while loading the model", error);
      }
    );
  }, [modelIndex]);

  return (
    <div>
      {isMobile ? <ARCanvas model={model} /> : <Canvas model={model} />}
      <div
        style={{ display: "flex", justifyContent: "center", padding: "1rem" }}
      >
        {LIST_THUMBNAILS.map((thumbnail, index) => (
          <Thumbnail
            key={index}
            src={thumbnail}
            isSelected={modelIndex === index}
            onClick={() => setModelIndex(index)}
          />
        ))}
      </div>
    </div>
  );
};
