import React from 'react';

type ThumbnailProps = {
  src: string;
  isSelected: boolean;
  onClick: () => void;
};

const Thumbnail: React.FC<ThumbnailProps> = ({ src, isSelected, onClick }) => {
  return (
    <img
      src={src}
      alt="Thumbnail"
      style={{
        width: '50px',
        height: '50px',
        margin: '0 5px',
        cursor: 'pointer',
        border: isSelected ? '2px solid blue' : 'none',
        boxShadow: isSelected ? '0 0 10px rgba(0, 0, 255, 0.5)' : 'none',
        transition: 'box-shadow 0.3s ease-in-out',
      }}
      onClick={onClick}
    />
  );
};

export default Thumbnail;
