/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useRef, useState } from "react";
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { ARButton } from "three/examples/jsm/webxr/ARButton.js";

type ARCanvasProps = {
  model: THREE.Group | null;
};

const ARCanvas: React.FC<ARCanvasProps> = ({ model }) => {
  const mountRef = useRef<HTMLDivElement>(null);
  const rendererRef = useRef<THREE.WebGLRenderer | null>(null);
  const [isArMode, setIsArMode] = useState(false);
  useEffect(() => {
    if (!mountRef.current || !model) return;

    const mount = mountRef.current;
    const scene = new THREE.Scene();

    // Setup camera
    const camera = new THREE.PerspectiveCamera(
      23,
      mount.clientWidth / mount.clientHeight,
      0.1,
      1000
    );
    camera.position.set(1, 0.5, 0.5);

    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(mount.clientWidth, mount.clientHeight);
    renderer.setClearColor(0xcccccc);
    renderer.xr.enabled = true;
    mount.appendChild(renderer.domElement);

    // Store reference to renderer
    rendererRef.current = renderer;

    // Add AR button
    const arButton = ARButton.createButton(renderer);
    arButton.addEventListener("click", () => {
      setIsArMode(true);
      model.position.set(0, 0, -1);
      model.scale.set(1.5, 1.5, 1.5);
      model.rotation.set(0.75, -1, 0);
      camera.add(model);
      scene.add(camera);
    });
    document.body.appendChild(arButton);

    const ambientLight = new THREE.AmbientLight(0xffffff, 0.6);
    scene.add(ambientLight);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(5, 10, 7.5);
    scene.add(directionalLight);

    scene.add(model);

    const controls = new OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enableZoom = true;
    controls.autoRotate = true;
    controls.minDistance = 1;
    controls.maxDistance = 3;
    controls.mouseButtons = {
      LEFT: THREE.MOUSE.ROTATE,
      MIDDLE: THREE.MOUSE.DOLLY,
      RIGHT: THREE.MOUSE.ROTATE,
    };
    controls.touches = {
      ONE: THREE.TOUCH.ROTATE,
      TWO: THREE.TOUCH.DOLLY_ROTATE,
    };

    const animate = () => {
      renderer.setAnimationLoop(() => {
        controls.update();
        renderer.render(scene, camera);
      });
    };

    animate();

    renderer.xr.addEventListener("sessionend", () => {
      setIsArMode(false);
      model.position.set(0, 0, 0);
      model.scale.set(1, 1, 1);
      model.rotation.set(0, 0, 0);
    });

    return () => {
      document.body.removeChild(arButton);
      mount.removeChild(renderer.domElement);
      scene.remove(model);
    };
  }, [model, isArMode]);

  return (
    <div>
      {isArMode ? (
        <div style={{ position: "absolute", top: 10, left: 10 }}>
          <button
            onClick={() => {
              setIsArMode(false);
              rendererRef.current?.xr.getSession()?.end();
              // Remove model from camera
              if (model) {
                model.position.set(0, 0, 0);
                model.scale.set(1, 1, 1);
                model.rotation.set(0, 0, 0);
              }

            }}
          >
            Exit AR
          </button>
        </div>
      ) : (
        <div
          ref={mountRef}
          style={{ width: "100%", height: "calc(100vh - 360px)" }}
        />
      )}
    </div>
  );
};

export default ARCanvas;
